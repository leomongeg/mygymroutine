//
//  MyGym.m
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 13/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "MyGym.h"

@implementation MyGym

+ (Boolean)isWhiteSpaceOrEmty:(NSString *)rawString andShowErrorMessaje:(Boolean)show
{
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed          = [rawString stringByTrimmingCharactersInSet:whitespace];
    if ([trimmed length] == 0)
    {
        if(show)
            [self showErrorMessage:@"Todos los campos son requeridos."];
        return true;
    }
    
    return false;
}

+ (Boolean) stringIsNumeric:(NSString *) str andShowErrorMessage:(Boolean)show
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSNumber *number             = [formatter numberFromString:str];
    formatter                    = nil;
    
    if(show && !number)
    {
        [self showErrorMessage:[NSString stringWithFormat:@"%@ no es un valor numerico valido", str]];
    }
    
    return !!number;
}

+ (void)showErrorMessage:(NSString *)mensaje
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!"
                                                    message:mensaje
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil, nil];
    [alert show];
    alert = nil;
}

+ (NSString *)stringDate:(NSDate *)date withFormat:(NSString *)format
{
    static NSDateFormatter *formatter = nil;
    
    if(formatter == nil)
    {
        formatter = [[NSDateFormatter alloc] init];
    }
    
    [formatter setDateFormat:format];
    
    return  [formatter stringFromDate:date];
}

+ (NSDateFormatter *)UTCDateFormatter
{
    static NSDateFormatter *dateFormatter = nil;
    
    if(dateFormatter != nil)
        return dateFormatter;
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *timeZone           = [NSTimeZone timeZoneWithName:@"GMT"];
    [dateFormatter setTimeZone:timeZone];
    
    return dateFormatter;
}

+ (NSDate *)getUTCFormateDate:(NSDate *)localDate
{
    return [[self UTCDateFormatter] dateFromString:[[self UTCDateFormatter] stringFromDate:localDate]];
}

+ (void)setUserFullName:(NSString *)name
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:name forKey:@"user_full_name"];
}

+ (NSString *)getUserFullName
{
    static NSString *fullname = nil;
    
    if(fullname != nil)
    {
        return fullname;
    }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    fullname              = [prefs objectForKey:@"user_full_name"];
    
    return fullname;
}

+ (void)setUsername:(NSString *)username
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:username forKey:@"user_username"];
}

+ (NSString *)getUsername
{
    static NSString *username = nil;
    
    if(username != nil)
    {
        return username;
    }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    username              = [prefs objectForKey:@"user_username"];
    
    return username;
}


@end
