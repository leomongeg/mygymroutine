//
//  CustomPresentationViewController.h
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 11/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPresentationViewController : UIPresentationController

@end
