//
//  ExerciceViewController.m
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 11/1/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "ExerciceViewController.h"

@interface ExerciceViewController ()

@property (nonatomic, weak) Exercise *exercise;

@end

@implementation ExerciceViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if(_exerciceTVC == nil)
    {
        _exerciceTVC = [[ExerciceTVC alloc] init];
        [_exerciceTVC setManagedObjectContext:_managedObjectContext];
        [_exerciceTVC setDay:_day];
        [_exerciceTVC setDelegate:self];
    }
    
    [_tblExercices setDataSource:_exerciceTVC];
    [_tblExercices setDelegate:_exerciceTVC];
    [_exerciceTVC setView:_exerciceTVC.tableView];
//    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)timerTick:(NSTimer *)timer {
    NSDate *now = [NSDate date];
    
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"h:mm:ss a";  // Formato  "8:47:22 AM"
    }
    _lblTImer.text = [dateFormatter stringFromDate:now];
}

#pragma mark - LMEntityTBLVCDelegate

- (void)didSelectedEntity:(id)entity
{
//    _exercise = (Exercise *)entity;
}

- (void)didEditEntity:(id)entity
{
    _exercise = (Exercise *)entity;
    [self performSegueWithIdentifier:@"GoNewExercice" sender:self];
}

- (void)didSelectedEntity:(id)entity kind:(NSString *)kind{}
- (void)didAddNewElement:(NSString *)kind{}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"GoNewExercice"])
    {
        NewExerciceViewController *newVC = segue.destinationViewController;
        [newVC setManagedObjectContext:_managedObjectContext];
        [newVC setDay:_day];
        [newVC setExercise:_exercise];
        [newVC setDelegate:self];
    }
}


- (IBAction)newExercice:(id)sender
{
    _exercise = nil;
    [self performSegueWithIdentifier:@"GoNewExercice" sender:self];
}

#pragma mark - NewExerciseDelegate

- (void)exerciseCreated:(Exercise *)exercise
{
    [_exerciceTVC reloadTableData];
    [_tblExercices reloadData];
}

@end
