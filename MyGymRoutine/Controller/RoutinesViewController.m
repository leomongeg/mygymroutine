//
//  RoutinesViewController.m
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 10/1/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "RoutinesViewController.h"
#import "SWRevealViewController.h"
#import "DaysViewController.h"
#import "AppDelegate.h"
#import "Usuario.h"
#import "UsuarioRepository.h"

@interface RoutinesViewController ()

@property (nonatomic) IBOutlet UIBarButtonItem* revealButtonItem;
@property (nonatomic, weak) Routine *routine;
@property (nonatomic) Boolean toEdit;
@property (nonatomic, weak) NSString *uname;
@property (nonatomic, weak) NSString *email;


@end

@implementation RoutinesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initManageObjectContext];
    
    if(_routineTVC == nil)
    {
        _routineTVC = [[RoutinesTVC alloc] init];
        [_routineTVC setDelegate:self];
        [_routineTVC setManagedObjectContext:_managedObjectContext];
    }
    
    [_rutinesTableView setDataSource:_routineTVC];
    [_rutinesTableView setDelegate:_routineTVC];
    [_routineTVC setView:_routineTVC.tableView];
    
    [self customSetup];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if(![[prefs objectForKey:@"user_has_logged"]boolValue])
    {
        [self loginUser];
    }
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.revealButtonItem setTarget: self.revealViewController];
        [self.revealButtonItem setAction: @selector( revealToggle: )];
        [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        if([self.revealViewController.rearViewController class] == [MenuNavViewController class])
        {
            [(MenuNavViewController*)self.revealViewController.rearViewController setDelegate:self];
        }
    }
}

- (void)initManageObjectContext
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _managedObjectContext    = [appDelegate managedObjectContext];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - LMEntityTBLVCDelegate

- (void)didSelectedEntity:(id)entity
{
    _routine = (Routine *)entity;
    _toEdit  = NO;
    [self performSegueWithIdentifier:@"GoDayVC" sender:self];
}

- (void)didEditEntity:(id)entity
{
    _routine = (Routine *)entity;
    _toEdit  = YES;
    [self performSegueWithIdentifier:@"newRoutine" sender:self];
}

- (void)didSelectedEntity:(id)entity kind:(NSString *)kind{}
- (void)didAddNewElement:(NSString *)kind{}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"newRoutine"])
    {
        NewRoutineViewController *newVC = (NewRoutineViewController *) segue.destinationViewController;
        [newVC setManagedObjectContext:_managedObjectContext];
        [newVC setDelegate:self];
        [newVC setIsEditing:_toEdit];
        if(_toEdit) [newVC setRoutine:_routine];
    }
    
    if([segue.identifier isEqualToString:@"GoDayVC"])
    {
        DaysViewController *dayVC = (DaysViewController *) segue.destinationViewController;
        [dayVC setRoutine:_routine];
        [dayVC setManagedObjectContext:_managedObjectContext];
    }
}


- (IBAction)addRoutine:(id)sender
{
    [self performSegueWithIdentifier:@"newRoutine" sender:self];
}

- (Usuario *)getUser
{
    NSString *username      = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    UsuarioRepository *repo = [[UsuarioRepository alloc]initWithManagedObjectContext:_managedObjectContext];
    
    return (Usuario *) [repo findOne:@"Usuario"
                          ByCriteria:@"username"
                     withStringValue:username
                     isCaseSensitive:YES];
}

- (void)loginUser
{
    
    Usuario * user = [self getUser];
    if(user == nil)
    {
        [self getUserInfo];
        return;
    }
    
    [MyGym setUserFullName:user.fullname];
    [MyGym setUsername:user.username];
}

- (void)getUserInfo
{
    if([self getUser] != nil) return;
    
    UIAlertView *alertView   = [[UIAlertView alloc]initWithTitle:@"Nuevo Usuario"
                                                         message:nil
                                                        delegate:self
                                               cancelButtonTitle:@"Cancelar"
                                               otherButtonTitles:@"Guardar", nil];
    alertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    UITextField *textField   = [alertView textFieldAtIndex:0];
    textField.placeholder    = @"Nombre";
    textField                = [alertView textFieldAtIndex:1];
    textField.placeholder    = @"Email";
    [textField setSecureTextEntry:NO];
    
    
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        UITextField *name  = [alertView textFieldAtIndex:0];
        UITextField *email = [alertView textFieldAtIndex:1];
        
        if ([MyGym isWhiteSpaceOrEmty:name.text andShowErrorMessaje:NO] &&
            [MyGym isWhiteSpaceOrEmty:email.text andShowErrorMessaje:NO])
        {
            [self showRegistrationErrors];
            return;
        }
        
        [self saveUserDataName:name.text andEmail:email.text];
    }
    
    if(buttonIndex == 0)
    {
        [self getUserInfo];
    }
}

- (void)getUserInfoForEdit
{
    Usuario *user            = [self getUser];
    UIAlertView *alertView   = [[UIAlertView alloc]initWithTitle:@"Editar"
                                                         message:nil
                                                        delegate:self
                                               cancelButtonTitle:@"Cancelar"
                                               otherButtonTitles:@"Guardar", nil];
    alertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    UITextField *textField   = [alertView textFieldAtIndex:0];
    [textField setText:user.fullname];
    textField                = [alertView textFieldAtIndex:1];
    [textField setText:user.email];
    [textField setSecureTextEntry:NO];
    
    
    [alertView show];
}

- (void)showRegistrationErrors
{
    UIAlertView *alertView   = [[UIAlertView alloc]initWithTitle:@"Error!!"
                                                         message:@"Todos los valores son requeridos."
                                                        delegate:self
                                               cancelButtonTitle:@"Aceptar"
                                               otherButtonTitles:nil, nil];
    [alertView show];
}

- (void)saveUserDataName:(NSString *)name andEmail:(NSString *)email
{
    NSString *username = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    Usuario *user      = [self getUser];
    
    if(user == nil)
        user = [NSEntityDescription insertNewObjectForEntityForName:@"Usuario" inManagedObjectContext:_managedObjectContext];
    
    [user setFullname:name];
    [user setEmail:email];
    [user setUsername:username];
    [user setCreated_at:[MyGym getUTCFormateDate:[NSDate date]]];
    [user setUpdated_at:[MyGym getUTCFormateDate:[NSDate date]]];
    [user setSync_at:[MyGym getUTCFormateDate:[NSDate date]]];
    
    [_managedObjectContext save:nil];
    [self loginUser];
}

#pragma mark - NewRoutineDelegate

- (void)routineCreated:(Routine *)routine
{
    [_routineTVC reloadTableData];
    [_rutinesTableView reloadData];
}

#pragma mark - MenuItemSelectedDelegate

- (void)onMenuItemSelected:(NSString *)itemSelected
{
    [self.revealViewController revealToggle:nil];
    
    if([itemSelected isEqualToString:@"first"])
    {
        [self getUserInfoForEdit];
        return;
    }

    UIAlertView *alertView   = [[UIAlertView alloc]initWithTitle:@"Proximamente!!!"
                                                         message:nil
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil, nil];
    [alertView show];
}

@end
