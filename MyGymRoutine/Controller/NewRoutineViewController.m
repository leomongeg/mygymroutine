//
//  NewRoutineViewController.m
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 11/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "NewRoutineViewController.h"
#import "CustomPresentationViewController.h"
#import "RoutineRepository.h"

@interface NewRoutineViewController ()

@property (nonatomic, retain) NSDate * curDate;
@property (nonatomic, retain) NSDateFormatter * formatter;
@property CGSize txtOriginalSize;

@end

@implementation NewRoutineViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    self.modalPresentationStyle = UIModalPresentationCustom;
    self.transitioningDelegate  = self;
    
    return self;
}

- (id)init
{
    self = [super init];
    
    self.modalPresentationStyle = UIModalPresentationCustom;
    self.transitioningDelegate = self;
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor colorWithRed:28/255.0
                                                green:31/255.0
                                                 blue:36/255.0
                                                alpha:0.56];
    self.curDate              = [NSDate date];
    self.formatter            = [[NSDateFormatter alloc] init];
    [_formatter setDateFormat:@"dd/MM/yyyy"];
    [_txtInfo setDelegate:self];
    [_txtInfo layoutIfNeeded];
    _txtOriginalSize          = _txtInfo.frame.size;
    [_lblDate setText:[_formatter stringFromDate:_curDate]];
    
    if(_isEditing) [self configureForEdit];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(KeyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(KeyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
 
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)configureForEdit
{
    [_txtInfo setText:_routine.information];
    _curDate = _routine.date;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source
{
    if(presented == self)
    {
        CustomPresentationViewController *cp = [[CustomPresentationViewController alloc]initWithPresentedViewController:presented presentingViewController:presenting];
        
        return cp;

    }
    return nil;
}

- (void)datePickerDonePressed:(THDatePickerViewController *)datePicker
{
    _curDate = datePicker.date;
    [_datePicker dismissSemiModalView];
    [_lblDate setText:[_formatter stringFromDate:_curDate]];

}

- (void)datePickerCancelPressed:(THDatePickerViewController *)datePicker
{
}

-(void)KeyboardWillHide:(NSNotification *)notification
{
    CGRect frame = _txtInfo.frame;
    frame.size   = _txtOriginalSize;
    [_txtInfo setFrame:frame];
    [_txtInfo setNeedsDisplay];
}

-(void)KeyboardWillShow:(NSNotification *)sender
{
    CGPoint kbPoint = [[[sender userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].origin;
    CGFloat txtMaxY = _txtInfo.frame.origin.y + _txtOriginalSize.height;
    CGFloat kbMinY  = self.view.frame.size.height - kbPoint.y;

    if(txtMaxY > kbMinY)
    {
        CGRect frame = _txtInfo.frame;;
        frame.size   = CGSizeMake(frame.size.width, frame.size.height - kbMinY/2);
        [_txtInfo setFrame:frame];
        [_txtInfo setNeedsDisplay];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)showDatePicker:(id)sender
{
    if(!_datePicker)
        _datePicker = [THDatePickerViewController datePicker];
    
    [_datePicker setDate:_curDate];
    [_datePicker setDelegate:self];
    [_datePicker setAllowClearDate:NO];
    [_datePicker setClearAsToday:YES];
    [_datePicker setAutoCloseOnSelectDate:YES];
    [_datePicker setAllowSelectionOfSelectedDate:YES];
    [_datePicker setDisableHistorySelection:NO];
    [_datePicker setDisableFutureSelection:NO];
    [_datePicker setSelectedBackgroundColor:[UIColor colorWithRed:125/255.0
                                                            green:208/255.0
                                                             blue:0/255.0
                                                            alpha:1.0]];
    
    [_datePicker setCurrentDateColor:[UIColor colorWithRed:242/255.0
                                                     green:121/255.0
                                                      blue:53/255.0
                                                     alpha:1.0]];
    
    [_datePicker setCurrentDateColorSelected:[UIColor yellowColor]];
    
    [_datePicker setDateHasItemsCallback:^BOOL(NSDate *date) {
        int tmp = (arc4random() % 30)+1;
        return (tmp % 5 == 0);
    }];
    [self.view endEditing:YES];
    //[self.datePicker slideUpInView:self.view withModalColor:[UIColor lightGrayColor]];
    [self presentSemiViewController:self.datePicker withOptions:@{
                                                                  KNSemiModalOptionKeys.pushParentBack    : @(NO),
                                                                  KNSemiModalOptionKeys.animationDuration : @(1.0),
                                                                  KNSemiModalOptionKeys.shadowOpacity     : @(0.3),
                                                                  }];
}

- (IBAction)saveData:(id)sender
{
    RoutineRepository *repo = [[RoutineRepository alloc] initWithManagedObjectContext:_managedObjectContext];
    
    if (_isEditing)
    {
        [_routine setInformation:_txtInfo.text];
        [_routine setDate:_curDate];
        [_routine setUpdated_at:[MyGym getUTCFormateDate:[NSDate date]]];
        [_managedObjectContext save:nil];
        [self routineCreated:nil];
    }else
    {
        [self routineCreated:[repo newRoutine:_curDate andInformation:_txtInfo.text]];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_txtInfo setNeedsDisplay];
}

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Protocol NewRoutine

- (void)routineCreated:(Routine *)routine
{
    id<NewRoutineDelegate> delegate = self.delegate;
    
    if ([delegate respondsToSelector:@selector(routineCreated:)])
    {
        [delegate routineCreated:routine];
    }
}

- (void)dealloc
{
    
}
@end
