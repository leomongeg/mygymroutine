//
//  DaysTVC.m
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 11/1/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "DaysTVC.h"

@interface DaysTVC ()

@property (nonatomic, weak) UITableView *realTableView;

@end

@implementation DaysTVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self reloadTableData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(_realTableView == nil) _realTableView = tableView;
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SWTableViewCell *cell = (SWTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"DayCell" forIndexPath:indexPath];
    Day *day              = (Day *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    UILabel *label        = (UILabel *) [cell viewWithTag:10001];
    
    [label setText:day.name];
    label = nil;
    
    if(cell.rightUtilityButtons == nil)
    {
        cell.rightUtilityButtons = [self rightButtons];
        [cell setDelegate:self];
    }
    
    return cell;
}

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                                title:@"Edit"];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                title:@"Delete"];
    
    return rightUtilityButtons;
}

#pragma mark - SWTableViewCellDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    NSIndexPath *indexPath = [_realTableView indexPathForCell:cell];
    
    switch (index)
    {
        case 0:
            [self didEditEntity:[self.fetchedResultsController objectAtIndexPath:indexPath]];
            break;
        case 1:
        {
            NSManagedObject *managedObject = [[self getFetchedResultsController] objectAtIndexPath:indexPath];
            [self.managedObjectContext deleteObject:managedObject];
            [self.managedObjectContext save:nil];
            break;
        }
        default:
            break;
    }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    return YES;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self didSelectedEntity:[self.fetchedResultsController objectAtIndexPath:indexPath]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CoreData FetchedResultsController

- (NSFetchedResultsController *)getFetchedResultsController
{
    if (self.fetchedResultsController != nil)
    {
        return self.fetchedResultsController;
    }
    
    NSPredicate *predicate       = [NSPredicate predicateWithFormat:@"routine = %@", _routine];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity  = [NSEntityDescription
                                    entityForName:@"Day" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    
    NSSortDescriptor *sort       = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                               ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    [fetchRequest setFetchBatchSize:20];
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:nil
                                                   cacheName:nil];
    [self setFetchedResultsController:theFetchedResultsController];
    [self.fetchedResultsController setDelegate:self];
    
    fetchRequest = nil;
    entity       = nil;
    
    return self.fetchedResultsController;
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    if (type == NSFetchedResultsChangeDelete)
    {
        [_realTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

@end
