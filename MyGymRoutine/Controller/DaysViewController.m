//
//  DaysViewController.m
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 11/1/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "DaysViewController.h"
#import "DayRepository.h"
#import "ExerciceViewController.h"

@interface DaysViewController ()

@property (nonatomic, weak) Day *day;
@property (nonatomic) Boolean isNew;

@end

@implementation DaysViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if(_daysTVC == nil)
    {
        _daysTVC = [[DaysTVC alloc] init];
        [_daysTVC setDelegate:self];
        [_daysTVC setRoutine:_routine];
        [_daysTVC setManagedObjectContext:_managedObjectContext];
    }
    
    [_tblDays setDataSource:_daysTVC];
    [_tblDays setDelegate:_daysTVC];
    [_daysTVC setView:_daysTVC.tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"GoExercicesVC"])
    {
        ExerciceViewController *exerciseVC = segue.destinationViewController;
        [exerciseVC setManagedObjectContext:_managedObjectContext];
        [exerciseVC setDay:_day];
    }
}


- (IBAction)newDay:(id)sender
{
    _isNew = YES;
    [self showUserDialog];
}

- (void)showUserDialog
{
    UIAlertView *alertView   = [[UIAlertView alloc]initWithTitle:@"Ingrese el identificador para el día"
                                                         message:nil
                                                        delegate:self
                                               cancelButtonTitle:@"Cancelar"
                                               otherButtonTitles:@"Guardar", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *textField   = [alertView textFieldAtIndex:0];
    textField.placeholder    = @"Identificador";
    
    if(!_isNew)
        [textField setText:_day.name];
    
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        UITextField *textField = [alertView textFieldAtIndex:0];
        
        if ([MyGym isWhiteSpaceOrEmty:textField.text andShowErrorMessaje:YES]) return;
        
        if(_isNew)
        {
            DayRepository *repo = [[DayRepository alloc] initWithManagedObjectContext:_managedObjectContext];
            [repo newWithName:textField.text andRoutine:_routine];
        }else
        {
            [_day setName:textField.text];
            [_day setUpdated_at:[MyGym getUTCFormateDate:[NSDate date]]];
            [_managedObjectContext save:nil];
        }
        
        [_daysTVC reloadTableData];
        [_tblDays reloadData];
    }
}

#pragma mark - LMEntityTBLVCDelegate

- (void)didSelectedEntity:(id)entity
{
    _day = (Day *)entity;
    [self performSegueWithIdentifier:@"GoExercicesVC" sender:self];
}

- (void)didEditEntity:(id)entity
{
    _day   = (Day *)entity;
    _isNew = NO;
    [self showUserDialog];
}

- (void)didSelectedEntity:(id)entity kind:(NSString *)kind{}
- (void)didAddNewElement:(NSString *)kind{}

@end
