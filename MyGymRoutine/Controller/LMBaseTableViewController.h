//
//  LMBaseTableViewController.h
//  CoopeServidores
//
//  Created by Jorge Leonardo Monge García on 9/10/14.
//  Copyright (c) 2014 LumenUp. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol LMEntityTBLVCDelegate;

@interface LMBaseTableViewController : UITableViewController

@property (nonatomic, weak) id<LMEntityTBLVCDelegate> delegate;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void)reloadTableData;
- (void)didSelectedEntity:(id)entity;
- (void)didSelectedEntity:(id)entity kind:(NSString *)kind;
- (void)didAddNewElement:(NSString *)kind;
- (void)didEditEntity:(id)entity;

- (void)setSelectedRowAtObject:(id)entity inTableView:(UITableView *)tableView animated:(Boolean)animated;

@end

@protocol LMEntityTBLVCDelegate <NSObject>

- (void)didSelectedEntity:(id)entity;
- (void)didSelectedEntity:(id)entity kind:(NSString *)kind;
- (void)didAddNewElement:(NSString *)kind;
- (void)didEditEntity:(id)entity;

@end