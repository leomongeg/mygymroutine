//
//  ExerciceViewController.h
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 11/1/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewExerciceViewController.h"
#import "ExerciceTVC.h"
#import "Day.h"
#import "Exercise.h"

@interface ExerciceViewController : UIViewController <LMEntityTBLVCDelegate, NewExerciseDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblExercices;
@property (weak, nonatomic) IBOutlet UILabel *lblTImer;
@property (nonatomic, strong) ExerciceTVC *exerciceTVC;
@property (nonatomic, weak) Day *day;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

- (IBAction)newExercice:(id)sender;

@end
