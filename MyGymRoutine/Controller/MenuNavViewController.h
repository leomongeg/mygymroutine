//
//  MenuNavViewController.h
//  Servifinanzas
//
//  Created by Jorge Leonardo Monge García on 18/12/14.
//  Copyright (c) 2014 LumenUp. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MenuItemSelectedDelegate;

@interface MenuNavViewController : UITableViewController

@property (nonatomic, weak) id<MenuItemSelectedDelegate> delegate;

@end

@protocol MenuItemSelectedDelegate <NSObject>

- (void)onMenuItemSelected: (NSString *)itemSelected;

@end
