//
//  NewRoutineViewController.h
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 11/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "THDatePickerViewController.h"
#import "NoteView.h"
#import "Routine.h"

@protocol NewRoutineDelegate;

@interface NewRoutineViewController : UIViewController<UIViewControllerTransitioningDelegate, THDatePickerDelegate, UITextViewDelegate>

@property (nonatomic, weak) id<NewRoutineDelegate> delegate;
@property (nonatomic, weak)IBOutlet NoteView *txtInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (nonatomic, strong) THDatePickerViewController * datePicker;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, weak) Routine *routine;
@property (nonatomic) Boolean isEditing;

- (IBAction)showDatePicker:(id)sender;
- (IBAction)saveData:(id)sender;
- (IBAction)close:(id)sender;

@end

@protocol NewRoutineDelegate <NSObject>

- (void)routineCreated:(Routine *)routine;

@end