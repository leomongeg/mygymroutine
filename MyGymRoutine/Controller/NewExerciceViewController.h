//
//  NewExerciceViewController.h
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 14/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YIPopupTextView.h"
#import "PopoverView.h"
#import "Exercise.h"

@protocol NewExerciseDelegate;

@interface NewExerciceViewController : UIViewController <UIViewControllerTransitioningDelegate, UITextViewDelegate, YIPopupTextViewDelegate, PopoverViewDelegate>

@property (weak, nonatomic) id<NewExerciseDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *txtSeries;
@property (weak, nonatomic) IBOutlet UITextField *txtRepetitions;
@property (weak, nonatomic) IBOutlet UITextField *txtRest;
@property (weak, nonatomic) IBOutlet UITextField *txtWeight;
@property (weak, nonatomic) IBOutlet UITextField *txtMuscle;
@property (weak, nonatomic) IBOutlet UITextView *txtExercice;
@property (weak, nonatomic) IBOutlet UIButton *btnUnits;
@property (weak, nonatomic) IBOutlet UIView *modalContainer;

@property (weak, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) Exercise *exercise;
@property (weak, nonatomic) Day *day;

- (IBAction)close:(id)sender;
- (IBAction)selectUnit:(id)sender;
- (IBAction)save:(id)sender;

- (void)exerciseCreated:(Exercise *)exercise;

@end

@protocol NewExerciseDelegate <NSObject>

- (void)exerciseCreated:(Exercise *)exercise;

@end
