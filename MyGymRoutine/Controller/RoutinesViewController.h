//
//  RoutinesViewController.h
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 10/1/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoutinesTVC.h"
#import "NewRoutineViewController.h"
#import "MenuNavViewController.h"
#import "Routine.h"

@interface RoutinesViewController : UIViewController <LMEntityTBLVCDelegate, NewRoutineDelegate, MenuItemSelectedDelegate>

@property (weak, nonatomic) IBOutlet UITableView *rutinesTableView;
@property (nonatomic, strong) RoutinesTVC *routineTVC;

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

- (IBAction)addRoutine:(id)sender;

@end
