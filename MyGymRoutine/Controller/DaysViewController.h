//
//  DaysViewController.h
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 11/1/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DaysTVC.h"
#import "Routine.h"

@interface DaysViewController : UIViewController <LMEntityTBLVCDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblDays;
@property (nonatomic, strong) DaysTVC *daysTVC;
@property (nonatomic, weak) Routine *routine;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

- (IBAction)newDay:(id)sender;

@end
