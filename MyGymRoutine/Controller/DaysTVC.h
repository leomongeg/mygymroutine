//
//  DaysTVC.h
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 11/1/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LMBaseTableViewController.h"
#import "Routine.h"
#import "Day.h"
#import "SWTableViewCell.h"

@interface DaysTVC : LMBaseTableViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, NSFetchedResultsControllerDelegate, SWTableViewCellDelegate>

@property (nonatomic, weak) Routine *routine;

@end