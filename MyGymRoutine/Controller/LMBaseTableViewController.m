//
//  LMBaseTableViewController.m
//  CoopeServidores
//
//  Created by Jorge Leonardo Monge García on 9/10/14.
//  Copyright (c) 2014 LumenUp. All rights reserved.
//

#import "LMBaseTableViewController.h"

@interface LMBaseTableViewController ()

@end

@implementation LMBaseTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    self = [super init];
    
    if(self)
    {
        _managedObjectContext = managedObjectContext;
    }
    
    return self;
}

- (void)reloadTableData
{
    [self setFetchedResultsController:nil];
    NSError *fecthResultError;
    if(![[self getFetchedResultsController] performFetch:&fecthResultError])
    {
        NSLog(@"Error al cargar table: %@, %@", fecthResultError, [fecthResultError userInfo]);
    }
}

-(void)didSelectedEntity:(id)entity
{
    id<LMEntityTBLVCDelegate> delegate = self.delegate;
    
    if ([delegate respondsToSelector:@selector(didSelectedEntity:)])
    {
        [delegate didSelectedEntity:entity];
    }
}

-(void)didSelectedEntity:(id)entity kind:(NSString *)kind
{
    id<LMEntityTBLVCDelegate> delegate = self.delegate;
    
    if ([delegate respondsToSelector:@selector(didSelectedEntity:kind:)])
    {
        [delegate didSelectedEntity:entity kind:kind];
    }
}

-(void)didAddNewElement:(NSString *)kind
{
    id<LMEntityTBLVCDelegate> delegate = self.delegate;
    
    if ([delegate respondsToSelector:@selector(didAddNewElement:)])
    {
        [delegate didAddNewElement:kind];
    }
}

- (void)didEditEntity:(id)entity
{
    id<LMEntityTBLVCDelegate> delegate = self.delegate;
    
    if ([delegate respondsToSelector:@selector(didEditEntity:)])
    {
        [delegate didEditEntity:entity];
    }
}

-(void)setSelectedRowAtObject:(id)entity inTableView:(UITableView *)tableView animated:(Boolean)animated
{
    NSIndexPath *indexPath = [_fetchedResultsController indexPathForObject:entity];
    [tableView selectRowAtIndexPath:indexPath animated:animated scrollPosition:UITableViewScrollPositionTop];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 0;
}

#pragma mark - CoreData FetchedResultsController

- (NSFetchedResultsController *)getFetchedResultsController
{
    return nil;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)dealloc
{
    NSLog(@"DEALLOCATED %@", NSStringFromClass([self class]));
}

@end
