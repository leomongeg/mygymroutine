//
//  MenuNavViewController.m
//  Servifinanzas
//
//  Created by Jorge Leonardo Monge García on 18/12/14.
//  Copyright (c) 2014 LumenUp. All rights reserved.
//

#import "MenuNavViewController.h"
#import "SWRevealViewController.h"

@interface MenuNavViewController ()

@property (nonatomic, strong)    NSArray *menu;

@end

@implementation MenuNavViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _menu = @[@"first", @"second", @"third", @"salir"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [_menu count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [_menu objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    // Configure the cell...
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self onMenuItemSelected:[_menu objectAtIndex:indexPath.row]];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        
        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
            
            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
            [navController setViewControllers: @[dvc] animated: NO ];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
        
    }
}

- (void)onMenuItemSelected: (NSString *)itemSelected
{
    id<MenuItemSelectedDelegate> delegate = self.delegate;
    
    if ([delegate respondsToSelector:@selector(onMenuItemSelected:)])
    {
        [delegate onMenuItemSelected:itemSelected];
    }
}

- (void)dealloc
{
    NSLog(@"DEALLOC MENU NAV");
}

@end
