//
//  NewExerciceViewController.m
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 14/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "NewExerciceViewController.h"
#import "CustomPresentationViewController.h"

#define Units @[@"Kgs", @"Lbs"]

@interface NewExerciceViewController ()

@property (nonatomic, strong) PopoverView *recursivePV;

@end

@implementation NewExerciceViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    self.modalPresentationStyle = UIModalPresentationCustom;
    self.transitioningDelegate  = self;
    
    return self;
}

- (id)init
{
    self = [super init];
    
    self.modalPresentationStyle = UIModalPresentationCustom;
    self.transitioningDelegate = self;
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor colorWithRed:28/255.0
                                                green:31/255.0
                                                 blue:36/255.0
                                                alpha:0.56];
    [_txtExercice setDelegate:self];
    [_btnUnits layoutIfNeeded];
    
    if(_exercise != nil)
    {
        [self loadExerciseData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source
{
    if(presented == self)
    {
        CustomPresentationViewController *cp = [[CustomPresentationViewController alloc]initWithPresentedViewController:presented presentingViewController:presenting];
        
        return cp;
        
    }
    return nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)selectUnit:(id)sender
{
    _recursivePV = [PopoverView showPopoverAtPoint:_btnUnits.center
                                            inView:_modalContainer
                                         withTitle:nil
                                   withStringArray:Units
                                          delegate:self];
}

- (IBAction)save:(id)sender
{
    if([self isValidData]) [self saveData];
}

#pragma mark - YIPopupTextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)aTextView
{
    if(_txtExercice == aTextView)
    {
        YIPopupTextView* popupTextView = [[YIPopupTextView alloc] initWithPlaceHolder:@"Ejercicios"
                                                                             maxCount:1000
                                                                          buttonStyle:YIPopupTextViewButtonStyleRightDone];
        [popupTextView setDelegate:self];
        [popupTextView setCaretShiftGestureEnabled:YES];
        [popupTextView showInViewController:self];
        [popupTextView setText:_txtExercice.text];
        
        return NO;
    }
    
    return YES;
}

- (void)popupTextView:(YIPopupTextView *)textView willDismissWithText:(NSString *)text cancelled:(BOOL)cancelled
{
    [_txtExercice setText:text];
}

#pragma mark - PopoverViewDelegate Methods

- (void)popoverView:(PopoverView *)popoverView didSelectItemAtIndex:(NSInteger)index
{
    [_btnUnits setTitle:[Units objectAtIndex:index] forState:UIControlStateNormal];
    [_recursivePV performSelector:@selector(dismiss) withObject:nil afterDelay:0.5f];
}

- (void)popoverViewDidDismiss:(PopoverView *)popoverView
{
    _recursivePV = nil;
}

- (void)saveData
{
    if (_exercise == nil)
    {
        _exercise = [NSEntityDescription insertNewObjectForEntityForName:@"Exercise" inManagedObjectContext:self.managedObjectContext];
        [_exercise setCreated_at:[MyGym getUTCFormateDate:[NSDate date]]];
        [_exercise setSync_at:[MyGym getUTCFormateDate:[NSDate date]]];
    }
    
    [_exercise setSeries:[NSNumber numberWithInteger:[_txtSeries.text integerValue]]];
    [_exercise setRepetitions:[NSNumber numberWithInteger:[_txtRepetitions.text integerValue]]];
    [_exercise setRest:[NSNumber numberWithInteger:[_txtRest.text integerValue]]];
    [_exercise setWeight:[NSNumber numberWithInteger:[_txtWeight.text integerValue]]];
    [_exercise setMuscle:_txtMuscle.text];
    [_exercise setExercise:_txtExercice.text];
    [_exercise setWeightUnit:_btnUnits.titleLabel.text];
    [_exercise setDay:_day];
    [_exercise setUpdated_at:[MyGym getUTCFormateDate:[NSDate date]]];
    
    
    [_managedObjectContext save:nil];
    [self exerciseCreated:_exercise];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (Boolean)isValidData
{
    if([MyGym isWhiteSpaceOrEmty:_txtSeries.text andShowErrorMessaje:YES]) return NO;
    if([MyGym isWhiteSpaceOrEmty:_txtRepetitions.text andShowErrorMessaje:YES]) return NO;
    if([MyGym isWhiteSpaceOrEmty:_txtRest.text andShowErrorMessaje:YES]) return NO;
    if([MyGym isWhiteSpaceOrEmty:_txtWeight.text andShowErrorMessaje:YES]) return NO;
    if([MyGym isWhiteSpaceOrEmty:_txtMuscle.text andShowErrorMessaje:YES]) return NO;
    if([MyGym isWhiteSpaceOrEmty:_txtExercice.text andShowErrorMessaje:YES]) return NO;
    if(![MyGym stringIsNumeric:_txtSeries.text andShowErrorMessage:YES]) return NO;
    if(![MyGym stringIsNumeric:_txtRepetitions.text andShowErrorMessage:YES]) return NO;
    if(![MyGym stringIsNumeric:_txtRest.text andShowErrorMessage:YES]) return NO;
    if(![MyGym stringIsNumeric:_txtWeight.text andShowErrorMessage:YES]) return NO;
    
    return YES;
}

- (void)loadExerciseData
{
    [_txtSeries setText:[_exercise.series stringValue]];
    [_txtRepetitions setText:[_exercise.repetitions stringValue]];
    [_txtRest setText:[_exercise.rest stringValue]];
    [_txtWeight setText:[_exercise.weight stringValue]];
    [_txtMuscle setText:_exercise.muscle];
    [_txtExercice setText:_exercise.exercise];
    [_btnUnits setTitle:_exercise.weightUnit forState:UIControlStateNormal];
}

#pragma mark - Protocol NewExerciseDelegate

- (void)exerciseCreated:(Exercise *)exercise;
{
    id<NewExerciseDelegate> delegate = self.delegate;
    
    if ([delegate respondsToSelector:@selector(exerciseCreated:)])
    {
        [delegate exerciseCreated:exercise];
    }
}

@end
