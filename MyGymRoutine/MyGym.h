//
//  MyGym.h
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 13/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Usuario.h"

@interface MyGym : NSObject

+ (Boolean)isWhiteSpaceOrEmty:(NSString *)rawString andShowErrorMessaje:(Boolean)show;
+ (Boolean) stringIsNumeric:(NSString *) str andShowErrorMessage:(Boolean)show;
+ (void)showErrorMessage:(NSString *)mensaje;
+ (NSString *)stringDate:(NSDate *)date withFormat:(NSString *)format;
+ (NSDateFormatter *)UTCDateFormatter;
+ (NSDate *)getUTCFormateDate:(NSDate *)localDate;
+ (void)setUserFullName:(NSString *)name;
+ (NSString *)getUserFullName;
+ (void)setUsername:(NSString *)username;
+ (NSString *)getUsername;

@end
