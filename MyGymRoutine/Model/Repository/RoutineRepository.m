//
//  RoutineRepository.m
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 12/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "RoutineRepository.h"

@implementation RoutineRepository

- (Routine *)newRoutine: (NSDate *)date andInformation:(NSString *)info
{
    Routine *routine = [NSEntityDescription insertNewObjectForEntityForName:@"Routine" inManagedObjectContext:self.managedObjectContext];
    
    [routine setDate:date];
    [routine setInformation:info];
    [routine setCreated_at:[MyGym getUTCFormateDate:[NSDate date]]];
    [routine setUpdated_at:[MyGym getUTCFormateDate:[NSDate date]]];
    [routine setSync_at:[MyGym getUTCFormateDate:[NSDate date]]];
    [routine setUser:(Usuario *)[self findOne:@"Usuario"
                                   ByCriteria:@"username"
                              withStringValue:[MyGym getUsername]
                              isCaseSensitive:YES]];
    
    NSError *error = nil;
    [self.managedObjectContext save:&error];
    
    if(error != nil)
    {
        return nil;
    }
    
    return routine;
}

@end
