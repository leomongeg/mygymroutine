//
//  RoutineRepository.h
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 12/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "LMBaseRepository.h"
#import "Routine.h"

@interface RoutineRepository : LMBaseRepository

- (Routine *)newRoutine: (NSDate *)date andInformation:(NSString *)info;

@end
