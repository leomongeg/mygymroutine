//
//  LMBaseRepository.h
//  CoopeServidores
//
//  Created by Jorge Leonardo Monge García on 9/8/14.
//  Copyright (c) 2014 LumenUp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LMBaseRepository : NSObject

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

- (id)initWithManagedObjectContext:(NSManagedObjectContext *) managedObjectContext;

-(NSEntityDescription *)findOneByCriteria:(NSString *)criteria withValue:(id)value;
-(NSEntityDescription *)findOne:(NSString *)entity ByCriteria:(NSString *)criteria withValue:(id)value;
-(NSEntityDescription *)findOne:(NSString *)entity ByCriteria:(NSString *)criteria withStringValue:(NSString *)value isCaseSensitive:(BOOL)caseSensitive;
-(NSArray *)findAll;

@end
