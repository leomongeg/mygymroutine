//
//  DayRepository.h
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 13/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "LMBaseRepository.h"
#import "Day.h"
#import "Routine.h"

@interface DayRepository : LMBaseRepository

- (Day *)newWithName:(NSString *)name andRoutine:(Routine *)routine;

@end
