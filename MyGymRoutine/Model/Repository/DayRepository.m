//
//  DayRepository.m
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 13/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "DayRepository.h"

@implementation DayRepository

- (Day *)newWithName:(NSString *)name andRoutine:(Routine *)routine
{
    Day *day = [NSEntityDescription insertNewObjectForEntityForName:@"Day" inManagedObjectContext:self.managedObjectContext];
    
    [day setName:name];
    [day setRoutine:routine];
    [day setCreated_at:[MyGym getUTCFormateDate:[NSDate date]]];
    [day setUpdated_at:[MyGym getUTCFormateDate:[NSDate date]]];
    [day setSync_at:[MyGym getUTCFormateDate:[NSDate date]]];
    
    NSError *error = nil;
    [self.managedObjectContext save:&error];
    
    if(error != nil)
    {
        return nil;
    }
    
    return day;
}

@end
