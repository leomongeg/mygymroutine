//
//  Usuario.h
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 15/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Routine;

@interface Usuario : NSManagedObject

@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * fullname;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSDate * updated_at;
@property (nonatomic, retain) NSDate * created_at;
@property (nonatomic, retain) NSDate * sync_at;
@property (nonatomic, retain) NSSet *routines;
@end

@interface Usuario (CoreDataGeneratedAccessors)

- (void)addRoutinesObject:(Routine *)value;
- (void)removeRoutinesObject:(Routine *)value;
- (void)addRoutines:(NSSet *)values;
- (void)removeRoutines:(NSSet *)values;

@end
