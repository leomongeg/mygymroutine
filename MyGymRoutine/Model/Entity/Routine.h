//
//  Routine.h
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 15/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Day, Usuario;

@interface Routine : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * information;
@property (nonatomic, retain) NSDate * created_at;
@property (nonatomic, retain) NSDate * updated_at;
@property (nonatomic, retain) NSDate * sync_at;
@property (nonatomic, retain) NSSet *days;
@property (nonatomic, retain) Usuario *user;
@end

@interface Routine (CoreDataGeneratedAccessors)

- (void)addDaysObject:(Day *)value;
- (void)removeDaysObject:(Day *)value;
- (void)addDays:(NSSet *)values;
- (void)removeDays:(NSSet *)values;

@end
