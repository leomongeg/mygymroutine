//
//  Day.h
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 15/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Exercise, Routine;

@interface Day : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDate * created_at;
@property (nonatomic, retain) NSDate * updated_at;
@property (nonatomic, retain) NSDate * sync_at;
@property (nonatomic, retain) NSSet *exercices;
@property (nonatomic, retain) Routine *routine;
@end

@interface Day (CoreDataGeneratedAccessors)

- (void)addExercicesObject:(Exercise *)value;
- (void)removeExercicesObject:(Exercise *)value;
- (void)addExercices:(NSSet *)values;
- (void)removeExercices:(NSSet *)values;

@end
