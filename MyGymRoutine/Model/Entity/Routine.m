//
//  Routine.m
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 15/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "Routine.h"
#import "Day.h"
#import "Usuario.h"


@implementation Routine

@dynamic date;
@dynamic information;
@dynamic created_at;
@dynamic updated_at;
@dynamic sync_at;
@dynamic days;
@dynamic user;

@end
