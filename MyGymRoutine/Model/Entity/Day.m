//
//  Day.m
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 15/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "Day.h"
#import "Exercise.h"
#import "Routine.h"


@implementation Day

@dynamic name;
@dynamic created_at;
@dynamic updated_at;
@dynamic sync_at;
@dynamic exercices;
@dynamic routine;

@end
