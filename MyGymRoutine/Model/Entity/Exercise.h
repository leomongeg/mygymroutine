//
//  Exercise.h
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 15/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Day;

@interface Exercise : NSManagedObject

@property (nonatomic, retain) NSString * exercise;
@property (nonatomic, retain) NSString * muscle;
@property (nonatomic, retain) NSNumber * repetitions;
@property (nonatomic, retain) NSNumber * rest;
@property (nonatomic, retain) NSNumber * series;
@property (nonatomic, retain) NSNumber * weight;
@property (nonatomic, retain) NSString * weightUnit;
@property (nonatomic, retain) NSDate * created_at;
@property (nonatomic, retain) NSDate * updated_at;
@property (nonatomic, retain) NSDate * sync_at;
@property (nonatomic, retain) Day *day;

@end
