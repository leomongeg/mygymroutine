//
//  Usuario.m
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 15/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "Usuario.h"
#import "Routine.h"


@implementation Usuario

@dynamic email;
@dynamic fullname;
@dynamic password;
@dynamic username;
@dynamic updated_at;
@dynamic created_at;
@dynamic sync_at;
@dynamic routines;

@end
