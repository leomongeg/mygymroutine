//
//  Exercise.m
//  MyGymRoutine
//
//  Created by Jorge Leonardo Monge García on 15/2/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "Exercise.h"
#import "Day.h"


@implementation Exercise

@dynamic exercise;
@dynamic muscle;
@dynamic repetitions;
@dynamic rest;
@dynamic series;
@dynamic weight;
@dynamic weightUnit;
@dynamic created_at;
@dynamic updated_at;
@dynamic sync_at;
@dynamic day;

@end
